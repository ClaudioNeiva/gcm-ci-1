package pacote1;

import org.junit.Assert;
import org.junit.Test;

public class Classe1Test {

	@Test
	public void teste1() {

		Classe1 obj1 = new Classe1();

		Integer resultadoEsperado = 20;

		Integer resultadoAtual = obj1.metodo1();

		Assert.assertEquals(resultadoEsperado, resultadoAtual);
	}

}
